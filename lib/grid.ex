defmodule Grid do
  def generate(x \\ 10, y \\ 10) do
    list = []
    x_axis = Enum.map(1..x, fn _ -> [0 | list] end)

    Enum.map(
      1..y,
      fn x ->
        List.flatten([x_axis | list])
      end
    )
  end
end
